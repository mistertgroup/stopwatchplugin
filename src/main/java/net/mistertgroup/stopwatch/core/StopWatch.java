package net.mistertgroup.stopwatch.core;

import java.util.HashMap;

/**
 * ストップウォッチのコア部分(プラグイン以外にも流用可能)<br>
 * タイマーは、nameごとに管理されています。
 *
 * @author misterT2525
 */
public class StopWatch {

    private HashMap<String, String> starttime;

    public StopWatch() {
        starttime = new HashMap<String, String>();
    }

    /**
     * ストップウォッチを開始します(既に起動してる場合はリセットして開始する)
     *
     * @param name
     */
    public void start(String name) {
        long time = System.currentTimeMillis();
        starttime.put(name, Long.toString(time));
    }

    /**
     * ストップウォッチを停止します
     *
     * @param name
     * @return 起動してない場合は-1が返ってきます
     */
    public long stop(String name) {
        long endtime = System.currentTimeMillis();
        String starttimeStr = starttime.get(name);
        if (starttimeStr == null) {
            return -1L;
        }
        starttime.remove(name);//終了したので
        long starttime = Long.parseLong(starttimeStr);
        return endtime - starttime;
    }

    /**
     * ストップウォッチの途中経過を返します
     *
     * @param name
     * @return 動作してなければ-1が返ってきます
     */
    public long now(String name) {
        String time = starttime.get(name);
        if (time == null) {
            return -1L;
        }
        return Long.parseLong(time);
    }
}
