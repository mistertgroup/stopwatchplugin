package net.mistertgroup.stopwatch.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * ランキングを作成します
 *
 * @author misterT2525
 */
public class Ranking {

    private final List<RankingData> datas;

    public Ranking() {
        datas = new ArrayList();
    }

    /**
     * データを追加します(同名のデータが存在する場合上書きします)
     *
     * @param data
     */
    public void addData(RankingData data) {
        //同名のデータがあれば、リストから削除
        String name = data.getName();
        for (RankingData old : datas) {
            if (name.equalsIgnoreCase(old.getName())) {
                datas.remove(old);
            }
        }

        datas.add(data);
        check();
    }

    /**
     * データを削除します
     *
     * @param name
     */
    public void removeData(String name) {
        for (RankingData data : datas) {
            if (name.equalsIgnoreCase(data.getName())) {
                datas.remove(data);
                check();
            }
        }
    }

    /**
     * データのスコアを更新します
     *
     * @param name
     * @param score
     */
    public void setScore(String name, long score) {
        for (RankingData data : datas) {
            if (name.equalsIgnoreCase(data.getName())) {
                data.setScore(score);
                check();
            }
        }
    }

    /**
     * データを取得します
     *
     * @param name
     * @return
     */
    public RankingData getData(String name) {
        for (RankingData data : datas) {
            if (name.equalsIgnoreCase(data.getName())) {
                return data;
            }
        }
        return null;
    }

    /**
     * 全てのデータを取得します
     *
     * @return
     */
    public List<RankingData> getAllData() {
        return datas;
    }

    /**
     * ランキングに使用するデータクラスです
     */
    public static class RankingData {

        private final String name;
        private long score = -1L;
        private int rank = -1;

        /**
         * nameのデータを作成します
         *
         * @param name
         */
        public RankingData(String name) {
            this.name = name;
        }

        /**
         * このデータの名前を取得します
         *
         * @return
         */
        public String getName() {
            return name;
        }

        /**
         * このデータのスコアを取得します
         *
         * @return
         */
        public long getScore() {
            return score;
        }

        /**
         * このデータのランクを取得します
         *
         * @return
         */
        public int getRank() {
            return rank;
        }

        /**
         * このデータのスコアを変更します
         *
         * @param score
         */
        public void setScore(long score) {
            this.score = score;
        }

        /**
         * このデータのランクを変更します
         *
         * @param rank
         */
        public void setRank(int rank) {
            this.rank = rank;
        }

    }

    private static final RankingComparator compare = new RankingComparator();

    /**
     * ランキングを更新します(特別な理由が無い限り利用しないでください)
     */
    public void check() {
        Collections.sort(datas, compare);

        Iterator<RankingData> it = datas.iterator();
        int rank = 1;
        while (it.hasNext()) {
            RankingData data = it.next();
            data.setRank(rank);
            rank++;
        }
    }

    private static class RankingComparator implements Comparator<RankingData> {

        @Override
        public int compare(RankingData a, RankingData b) {

            if (a.getScore() > b.getScore()) {
                return 1;
            } else if (a.getScore() == b.getScore()) {
                return 0;
            } else {
                return -1;
            }

        }

    }

}
