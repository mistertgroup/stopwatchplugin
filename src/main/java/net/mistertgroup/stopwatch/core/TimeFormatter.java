package net.mistertgroup.stopwatch.core;

/**
 * UNIX式の時間をxxxx時間xx分xx.xxx秒の形式にします
 *
 * @author misterT2525
 */
public class TimeFormatter {

    /**
     * フォーマットします
     *
     * @param time
     * @return
     */
    public static String format(long time) {
        int h = 0;//時間
        int m = 0;//分
        int s = 0;//秒
        String ms;//ミリ秒

        s = (int) (time / 1000L);
        if (s >= 60) {
            m = s / 60;
            s = s % 60;
        }
        if (m >= 60) {
            h = m / 60;
            m = m % 60;
        }

        String timeStr = Long.toString(time);
        ms = timeStr.substring(timeStr.length() - 3);

        StringBuilder sb = new StringBuilder();
        if (h != 0) {
            sb.append(h);
            sb.append("時間");
        }
        if (m != 0) {
            sb.append(m);
            sb.append("分");
        }
        sb.append(s);
        sb.append(".");
        sb.append(ms);
        sb.append("秒");

        return sb.toString();
    }
}
