package net.mistertgroup.stopwatch;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 * スポンジに乗ったときの処理を行います
 *
 * @author misterT2525
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onMove(PlayerMoveEvent event) {
        //動く前がスポンジなら二回動くことになるので、returnで終了させる
        {
            Location from = event.getFrom();
            from.add(0, -0.1, 0);//10cm下げる
            Block b = from.getBlock();
            if (b.getType().equals(Material.SPONGE)) {
                return;
            }
        }
        Location loc = event.getTo();
        loc.add(0, -0.1, 0);//10cm下げる
        Block b = loc.getBlock();
        //スポンジ以外ならretrunで終了させる
        if (!b.getType().equals(Material.SPONGE)) {
            return;
        }
        Player p = event.getPlayer();

        switch (b.getData()) {
            case (byte) 1:
                StopWatchUtil.toggle(p);
                break;
            case (byte) 2:
                StopWatchUtil.start(p);
                break;
            case (byte) 3:
                StopWatchUtil.stop(p);
                break;
        }

    }

}
