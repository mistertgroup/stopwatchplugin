package net.mistertgroup.stopwatch;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * コマンドに関するクラスです
 *
 * @author misterT2525
 */
public class StopWatchCommandExecutor implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            StopWatchUtil.help(sender);
        } else if (args[0].equalsIgnoreCase("ranking")) {
            StopWatchUtil.rank(sender);
        } else {
            StopWatchUtil.rank(sender, args[0]);
        }
        return true;
    }

}
