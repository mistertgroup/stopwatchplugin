package net.mistertgroup.stopwatch;

import java.util.Iterator;
import java.util.List;
import net.mistertgroup.stopwatch.core.Ranking;
import net.mistertgroup.stopwatch.core.Ranking.RankingData;
import net.mistertgroup.stopwatch.core.StopWatch;
import net.mistertgroup.stopwatch.core.TimeFormatter;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * ストップウォッチをBukkitで利用するためのツールです
 *
 * @author misterT2525
 */
public class StopWatchUtil {

    private final static StopWatch core = new StopWatch();
    private final static Ranking rank = new Ranking();

    private final static String prefix = "§a[StopWatch]";//メッセージの先頭

    /**
     * ストップウォッチを開始します
     *
     * @param p
     */
    public static void start(Player p) {
        core.start(p.getName());
        p.sendMessage(prefix + "ストップウォッチを開始しました。");
    }

    /**
     * ストップウォッチを停止します
     *
     * @param p
     */
    public static void stop(Player p) {
        long score = core.stop(p.getName());
        if (score == -1L) {
            return;
        }
        //ハイスコアかチェック
        RankingData data = rank.getData(p.getName());
        if (data == null) {//記録が存在しない場合
            Data d = new Data(p.getName());
            d.setScore(score);
            rank.addData(d);
            p.sendMessage(prefix + "自己ハイスコアを更新しました！");
        } else if (score < data.getScore()) {
            rank.setScore(p.getName(), score);
            p.sendMessage(prefix + "自己ハイスコアを更新しました！");
        }
        //タイムを表示
        p.sendMessage(prefix + "今回のタイムは、" + TimeFormatter.format(score) + "です。");
    }

    /**
     * ストップウォッチの状態をトグル方式で切り替えます
     *
     * @param p
     */
    public static void toggle(Player p) {
        if (core.now(p.getName()) != -1L) {
            start(p);
        } else {
            stop(p);
        }
    }

    /**
     * configから全てのデータを取り出します
     */
    public static void init() {
        ConfigurationSection score = StopWatchPlugin.getInstance().getConfig().getConfigurationSection("score");
        if (score == null) {
            return;
        }
        List<RankingData> list = rank.getAllData();
        list.clear();
        for (String name : score.getKeys(false)) {
            Data data = new Data(name);
            list.add(data);
        }
        rank.check();
    }

    /**
     * コマンドのヘルプを表示します
     *
     * @param sender
     */
    public static void help(CommandSender sender) {
        sender.sendMessage(prefix + "プラグイン制作者: misterT2525");
        sender.sendMessage(prefix + "プラグインバージョン: " + StopWatchPlugin.getInstance().getDescription().getVersion());
        sender.sendMessage(prefix + "/sw ranking: 上位10位のランキングを表示");
        sender.sendMessage(prefix + "/sw <playername>: <playername>のランクを表示");
    }

    /**
     * 上位10位のランキングを表示します
     *
     * @param sender
     */
    public static void rank(CommandSender sender) {
        Iterator<RankingData> it = rank.getAllData().iterator();
        while (it.hasNext()) {
            RankingData data = it.next();
            if (data.getRank() > 10) {
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(data.getRank());
            sb.append("位:");
            sb.append(data.getName());
            addSpace(sb, 20 - sb.length());
            sb.append(TimeFormatter.format(data.getScore()));
            sender.sendMessage(prefix + sb.toString());
        }
    }

    /**
     * nameさんの記録を表示します
     *
     * @param sender
     * @param name
     */
    public static void rank(CommandSender sender, String name) {
        RankingData data = rank.getData(name);
        if (data == null) {
            sender.sendMessage(prefix + "記録がありません。");
            return;
        }
        sender.sendMessage(prefix + name + "さんの記録");
        sender.sendMessage(prefix + TimeFormatter.format(data.getScore()) + "(" + data.getRank() + "位)");
    }

    private static void addSpace(StringBuilder sb, int length) {
        for (int i = 0; i < length; i++) {
            sb.append(" ");
        }
    }

    private static class Data extends Ranking.RankingData {

        public Data(String name) {
            super(name);
            //configからスコアを取り出す
            FileConfiguration config = StopWatchPlugin.getInstance().getConfig();
            if (config.contains("score." + name)) {
                setScore(config.getLong("score." + name));
            }
        }

        @Override
        public void setScore(long score) {
            super.setScore(score);
            //スコアをconfigに保存
            FileConfiguration config = StopWatchPlugin.getInstance().getConfig();
            config.set("score." + getName(), score);
        }
    }

}
