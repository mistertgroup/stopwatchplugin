package net.mistertgroup.stopwatch;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author misterT2525
 */
public class StopWatchPlugin extends JavaPlugin {

    private static StopWatchPlugin instance;

    public static StopWatchPlugin getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        saveConfig();
        StopWatchUtil.init();
        Bukkit.getPluginManager().registerEvents(new PlayerListener(), this);
        getCommand("stopwatch").setExecutor(new StopWatchCommandExecutor());
    }

    @Override
    public void onDisable() {
        saveConfig();
    }

}
